 //
//  ContentView.swift
//  FreshStart
//
//  Created by Jan Chaloupka on 10.08.2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, branch !")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
   
