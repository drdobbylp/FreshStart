//
//  FreshStartApp.swift
//  FreshStart
//
//  Created by Jan Chaloupka on 10.08.2021.
//

import SwiftUI

@main
struct FreshStartApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
